import { v } from 'voko'
import { Directory } from '../../directory'
import { centerMapOnSpace } from '../../map/toSpace'
import { showCard } from '../../card'
import { showModal } from '../../modal'

// there are two parts to rendering categories: draw placeholder dropdown
// headers and modal links. then create the actual dropdown content on click

const l = Directory.collections
const list = [
  {
    name: 'Offices',
    data: () => l.tags.get('office'),
  }, {
    name: 'Labs',
    data: () => l.tags.get('lab'),
  }, {
    name: 'Meeting rooms',
    data: () => l.tags.get('meeting-room'),
  }, {
    name: 'Clubs',
    data: () => l.tags.get('club'),
  }, {
    name: 'Staff',
    data: () => l.staff,
  }, {
    name: 'Departments',
    data: () => l.departments,
  },
]

// don't wait for Directory.ready - it should be loaded
const buildDropdown = dataFunction => {
  const data = typeof dataFunction === 'function'
    ? dataFunction()
    : dataFunction

  // departments are stored as a Map
  if (data instanceof Map) {
    const content = v('article')
    // a Map() doesn't have .map()
    data.forEach((value, key) => {
      const header = v(DropdownHeader, { name: key, data: value })
      content.appendChild(header)
    })
    return content
  }
  return v(NameRoomList, { iterable: data || [] })
}

const DropdownHeader = ({ name, data }) =>
  v('h2.cat', {
    onClick(ev) {
      const header = ev.target
      if (!header.classList.contains('loaded')) {
        header.classList.add('loaded')

        const next = header.nextElementSibling
        const article = buildDropdown(data)
        header.parentElement.insertBefore(article, next)
      }
      header.classList.toggle('open')
    },
  }, [
    // v(Icon, { icon: arrowSVG }),
    name,
  ])

const ModalHeader = ({ contentKey, ...other }, children) =>
  v('h2.cat.zoom', {
    onClick() {
      showModal(contentKey)
    },
    ...other,
  }, children)

// turn a Set() of objects with `room` and (optionally) `name` fields into DOM
const NameRoomList = ({ iterable }) => {
  // can't use `iterable.map` since Set() doesn't support it
  const article = v('article')
  iterable.forEach(entry => {
    const { name, room } = entry
    article.appendChild(
      v('.list-item category', {
        onClick() {
          centerMapOnSpace(room)
          showCard(entry)
        },
      }, [
        v('h3', name ? name : room),
        name && v('h4', room),
      ]))
  })
  return article
}

const Categories = attrs =>
  v('', attrs, [
    list.map(({ name, data }) =>
      v(DropdownHeader, { name, data })),
    v('.seperator'),
    v(ModalHeader, {
      contentKey: 'About',
      id: 'about', // for icon
    }, [
      // v(Icon, { icon: infoSVG }),
      'About',
    ]),
    v(ModalHeader, {
      contentKey: 'Emergency',
      id: 'emergency', // for icon
    }, [
      // v(Icon, { icon: warningSVG }),
      'Emergency',
    ]),
  ])

export { Categories }
