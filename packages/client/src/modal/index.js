import { v } from 'voko'
import { version } from '../../package.json'
import './style.css'

// you'll want to turn on line wrapping for this file
const live = {}

v('', {
  ref: e => live.AboutModal = e,
}, [
  v('h1', 'Relocate'),
  v('p', [
    'Interactive maps and directories for UVic buildings. Helps you be less lost.',
  ]),
  v('h3', 'Feedback'),
  v('p', [
    'Contributions welcome! There\'s surely mistakes and missing information. Contact me for anything at ', v('a[href=mailto:hamesg@uvic.ca]', 'hamesg@uvic.ca'), ' or file an issue on the GitLab linked below.',
  ]),
  v('h3', 'Design'),
  v('p', [
    'An offline ready progressive web application. All information is processed on your device, including search and rendering. Polygons and room data is saved on first visit, and map images are saved as they\'re requested.',
  ]),
  v('p', [
    'Built with ', v('a', { href: 'https://leafletjs.com' }, 'Leaflet.js'), ' in Spring 2018. Maintained on ', v('a', { href: 'https://gitlab.com/nthm/engr-directory' }, 'GitLab'),
  ]),
  v('small', `Version: v${version}`),
])

// generally, don't return an array. but this "isn't" a component
const row = ([name, number]) =>
  v('.row', [
    v('h3', name),
    v('p', number),
  ])

// data from UVic's website
v('', {
  ref: e => live.EmergencyModal = e,
}, [
  v('h1', 'Emergency Numbers'),
  v('.cols', [
    v('.col', [
      [ 'Ambulance, Fire, Police', '911' ],
      [ 'Campus Security', '250-721-7599' ],
      [ 'Victoria Women\'s Sexual Assault Centre', '250-383-3232' ],
      [ 'NEED Crisis Line', '250-386-6323' ],
      [ 'Poison Control Centre', '1-800-567-8911' ],
      [ 'Harrassment and Discrimination', '250-721-7007' ],
      [ 'Interfaith Chaplaincy', '250-721-8338' ],
    ].map(row)),
    v('.col', [
      [ 'Counselling Services', '250-721-8341' ],
      [ 'Health Services', '250-721-8492' ],
      [ 'Occupational Health and Safety', '250-721-8971' ],
      [ 'Ombudsperson', '250-721-8357' ],
      [ 'Peer Helping', '250-721-8343' ],
      [ 'Women\'s Centre', '250-721-8353' ],
      [ 'International and Exchange Student Services', '250-721-6361' ],
      [ 'Resource Centre for Students with a Disability', '250-472-4947' ],
    ].map(row)),
  ]),
])

// cannot use appendChild on template tags via v('template', [ ... ])
Object.keys(live).forEach(key => {
  const template = document.createElement('template')
  template.innerHTML = live[key].innerHTML
  live[key] = template
})

// init - shade and templates
const Modal = () =>
  v('#shade', {
    ref: e => live.ModalShade = e,
    onClick() {
      live.ModalShade.style = ''
      live.ModalShade.classList.remove('active')
    },
  }, v('#modal', { ref: e => live.Modal = e }))

const showModal = name => {
  const content = document.importNode(live[`${name}Modal`].content, true)
  live.Modal.innerHTML = ''
  live.Modal.appendChild(content)
  live.ModalShade.classList.add('active')
  setTimeout(() => {
    live.ModalShade.style = 'opacity: 1'
  }, 100)
}

export { Modal, showModal }
