import { requestDirectoryPayloads } from '../util/requests'

// index-based search library
// see docs/search.md

// relates tokens substrings to lists of spaces
const index = new Map()

const collections = {
  // room --> { space }
  spaces: new Map(),
  // room --> { Leaflet Polygon }
  polygons: new Map(),
  // set of [ { staff } ... ]
  staff: new Set(),
  // department code --> set of [ { space } ... ]
  departments: new Map(),
  // tag --> set of [ { space } ... ]
  tags: new Map(),
}

// this will be resolved when the index is built
let resolveReadyPromise
const ready = new Promise(resolve => {
  resolveReadyPromise = resolve
})

// for performance timing and counts of (un)linked objects
const stats = {}
const endTime = time => (window.performance.now() - time).toFixed(1) + ' ms'

// upsert a list of spaces stored under `token` in a collection (or the index)
function upsert(token, space, collection) {
  const currentList = collection.get(token) || new Set()
  collection.set(token, currentList.add(space))
}

// relate tokens and its substrings to a space in the index
function relate(fields, object) {
  // lowercase each term, split on spaces and a few things
  const sanitized = fields.map(x => {
    return x.toLowerCase().replace(/[,.:;!?-_()]/g, ' ').split(/\s+/)
  })
  // flatten array results
  const text = [].concat.apply([], sanitized)
  for (const token of new Set(text)) {
    for (let i = 0, len = token.length; i < len; i++) {
      // add every prefix substring to the index
      const substring = token.substr(0, i + 1)
      upsert(substring, object, index)
    }
  }
}

async function setupDirectory() {
  const { spaces, info, staff } = await requestDirectoryPayloads()

  // the `info` and `staff` payloads are ideally emptied during indexing
  Object.assign(stats, {
    spaces: spaces.length,
    info: Object.keys(info).length,
    staff: Object.keys(staff).length,
  })
  console.time('index')
  const startIndexTime = window.performance.now()

  for (const obj of spaces) {
    const room = obj.room
    collections.spaces.set(room, obj)

    // string in `fields` will be split up and used to relate the space object
    const fields = [room]

    let dept = obj.dept
    if (dept) {
      fields.push(dept)
      upsert(dept, obj, collections.departments)
    }

    if ('type' in obj) {
      fields.push(obj.type)
    }

    // lookup the room in the `info` payload (info.json)
    if (room in info) {
      // don't nest the info object, merge it
      for (const [key, val] of Object.entries(info[room])) {
        // this could be interestingly destructive
        obj[key] = val
        fields.push(val)
      }
      if ('tags' in info[room]) {
        // be less forgiving with spacing of tags
        info[room].tags.split(' ').forEach(tag => {
          upsert(tag, obj, collections.tags)
        })
      }
      delete info[room]
    }

    // relate the current fields before relating any from `staff`
    // this helps search result order
    relate(fields, obj)

    if (room in staff) {
      obj.staff = staff[room]
      delete staff[room]

      for (const person of obj.staff) {
        // reverse link the person to the space (but not circularly)
        person.room = room
        collections.staff.add(person)

        // order matters here:
        relate(Object.values(person), person) // first result
        relate([person.name], obj) // second result
      }
    }
  }

  console.timeEnd('index')
  stats.indexTime = endTime(startIndexTime)
  resolveReadyPromise()

  // useful for debugging
  for (const obj of [staff, info]) {
    const length = Object.keys(obj).length
    if (length > 0) {
      console.warn(`linked object has ${length} unmerged entries`, obj)
    }
  }
  console.log('stats:', stats)
  console.log('indexed', index.size, 'strings')
}

function search(query) {
  console.time('search')
  const startSearchTime = window.performance.now()
  const tokens = query.trim().toLowerCase().split(/\s+/)

  // initial token determines largest result set
  let results = index.get(tokens.shift()) || new Set()

  // for all subsequent tokens reduce the set
  for (const token of tokens) {
    const tokenSet = index.get(token) || new Set()
    results = new Set([...results].filter(x => tokenSet.has(x)))
  }

  console.timeEnd('search')
  stats.searchTime = endTime(startSearchTime)
  return results
}

const Directory = Object.freeze({ index, collections, ready, search })

export { Directory, setupDirectory, stats }
