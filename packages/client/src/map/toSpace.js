import { LMap } from '.'
import { Elevator } from './elevator'
import { Directory } from '../directory'

// TODO: ideally, the level that a space is on would be a field in the space
// object but that would involve changing the schema - again
async function centerMapOnSpace(room) {
  // this has an issue with "ELW SN" where it will go to the Nth level's
  // stairwell. it will work for now
  const levelIndex = room.search(/\d/)
  if (levelIndex < 0) {
    console.warn(`space ${room} doesn't contain a level? refusing`)
    return
  }
  // pull the 1 off "ELW 123"
  let level = parseInt(room.charAt(levelIndex))
  if (room.startsWith('EOW')) {
    level -= 1
  }
  Elevator.level = level
  await Elevator.loading // wouldn't need to do this if it wasn't a Proxy()...
  const polygon = Directory.collections.polygons.get(room)
  LMap.flyToBounds(polygon.getBounds())
}

export { centerMapOnSpace }
