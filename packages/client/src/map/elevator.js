// move between floors by changing layers of tiles and polygons

import { LMap, MapTileLayer } from '.'
import { loadLevelIntoLayer } from './polygons/load'

const schema = Object.seal({
  // the Promise that resolves when the level is finished loading
  loading: null,
  // set as null since set() needs to run when the map is ready
  level: null,
  // NOTE: some cultures skip numbers, ex: '1, 2, 3, 5'?
  // well...EOW is out of sync with ELW/ECS - you can't win
  minLevel: 0,
  maxLevel: 7,
  // can't use layers[level] because need to know the previous layer...
  layer: null,
  // polygon layers - might not load in order so `{ [0-6]: LayerGroup }`
  layers: {},
})

const Elevator = new Proxy(schema, {
  async set(obj, prop, value) {
    if (prop !== 'level'
      || value === obj.level
      || value < obj.minLevel
      || value > obj.maxLevel
    ) {
      // don't change anything
      return false
    }
    let resolvePromise
    // this will be resolved when the floor changes
    obj.loading = new Promise(resolve => {
      resolvePromise = resolve
    })
    obj.level = value
    // avoid extra TileLayers using setUrl() to redraw on the existing layer
    // TODO: bundle all URLs to one place? similar to util/requests
    MapTileLayer.setUrl(`/data/engineering/tiles/${value}/{z}/{x}/{y}.png`)
    if (obj.layer) {
      LMap.removeLayer(obj.layer)
    }
    if (!(value in obj.layers)) {
      const layer = await loadLevelIntoLayer(value)
      obj.layers[value] = layer
    }
    obj.layer = obj.layers[value]
    if (obj.layer) {
      LMap.addLayer(obj.layer)
    }
    resolvePromise()
    return true
  },
})

export { Elevator }
