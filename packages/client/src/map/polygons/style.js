// produces styles for polygons

// takes a fully merged details object and styles it
function styleFromDetails(spaceData) {
  return {
    weight: 2,
    opacity: 0.1,
    // could differentiate between 'fillColor' and 'color'
    color: colourFromSpaceType(spaceData.type),
  }
}

function colourFromSpaceType(type) {
  // recommend modifying these live with webpack-dev-server
  const colours = [
    '#a6cee3',
    '#1f78b4',
    '#b2df8a',
    '#33a02c',
    '#fb9a99',
    '#e31a1c',
    '#fdbf6f',
    '#ff7f00',
    '#304ffe',
    '#ff4081',
    '#7c4dff',
    '#b15928',
    '#1b9e77',
    '#d95f02',
    '#7570b3',
    '#e7298a',
    '#66a61e',
    '#e6ab02',
    '#a6761d',
    '#666666',
  ]
  // `cat spaces.json  | jq ".[] | .type" | sort | uniq`
  const types = [
    'Class',
    'Class/Lab',
    'Class/Lab (scheduled)',
    'Class support',
    'Class (tiered)',
    'Computing',
    'Faculty office',
    'Food',
    'Graduate student office',
    'Janitorial',
    'Lab',
    'Lab support',
    'Mechanical and Electrical',
    'Office',
    'Office (other)',
    'Pending assignment',
    'Public',
    'Staff office',
    'Student support',
    'Washroom',
  ]
  const index = types.indexOf(type)
  return index !== -1 ? colours[index] : '#000'
}

// used for colouring polygons on the map
// https://stackoverflow.com/q/5560248/lighten-or-darken-hex
function shadeHex(color, percent) {
  let f, t, p
  f = parseInt(color.slice(1), 16)
  t = percent < 0 ? 0 : 255
  p = percent < 0 ? percent * -1 : percent

  let R, G, B
  R = f >> 16
  G = f >> 8 & 0x00FF
  B = f & 0x0000FF

  return '#' + (
    0x1000000
    + (Math.round((t - R) * p) + R) * 0x10000
    + (Math.round((t - G) * p) + G) * 0x100
    + (Math.round((t - B) * p) + B) * 0x1
  ).toString(16).slice(1)
}

export { styleFromDetails, shadeHex }
