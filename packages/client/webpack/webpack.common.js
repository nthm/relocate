const HtmlPlugin = require('html-webpack-plugin')
const SpritePlugin = require('svg-sprite-loader/plugin')
const MiniCSSExtractPlugin = require('mini-css-extract-plugin')
const path = require('path')

const root = path.join(__dirname, '..')
const buildDir = path.join(root, 'build')
const entryFile = path.join(root, 'src', 'index.js')
const templateFile = path.join(root, 'src', 'index.ejs')

const config = {
  entry: entryFile,
  devtool: 'source-map',
  bail: true,
  output: {
    // there are no chunks in this project
    path: buildDir,
    filename: 'bundle.js',
  },
  plugins: [
    new MiniCSSExtractPlugin({
      filename: '[name].css',
    }),
    new HtmlPlugin({
      minify: {
        collapseWhitespace: true,
        removeScriptTypeAttributes: true,
        removeRedundantAttributes: true,
        removeStyleLinkTypeAttributes: true,
        removeComments: true,
      },
      template: templateFile,
    }),
    new SpritePlugin({
      plainSprite: true,
    }),
  ],
  module: {
    rules: [
      {
        test: /\.js$/,
        include: '/src',
        use: 'eslint-loader',
      },
      {
        test: /\.css$/,
        use: [MiniCSSExtractPlugin.loader, 'css-loader?-url'],
      },
      {
        test: /\.svg$/,
        use: {
          loader: 'svg-sprite-loader',
          options: {
            extract: false, // see github svg-sprite-loader/issues/123
          },
        },
      },
      {
        test: /\.(png|jpg|gif)$/,
        use: 'file-loader',
      },
    ],
  },
}

module.exports = config
