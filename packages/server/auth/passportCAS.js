// Updated passport-cas to use WHATWG url+fetch and only support CAS3.0
// Didn't implemented the CAS3.0 SAML support (attributes)

const AbstractStrategy = require('passport-strategy')
const util = require('util')
const fetch = require('node-fetch')

const parseString = util.promisify(require('xml2js').parseString)
const { normalize, stripPrefix } = require('xml2js/lib/processors')

const xmlParseOptions = {
  trim: true,
  normalize: true,
  explicitArray: false,
  tagNameProcessors: [normalize, stripPrefix],
}

function Strategy(options, verify) {
  AbstractStrategy.call(this)
  if (!options.idpURL || !options.spURL) {
    throw 'CAS3 Passport: Must provide `idpURL` (IdP) and `spURL` (SP) options'
  }
  if (!verify || typeof verify !== 'function') {
    throw 'CAS3 Passport: Must provide a `verify` callback'
  }
  this._verify = verify
  this.name = 'cas3'
  this.idpURL = options.idpURL
  this.spURL = options.spURL
}

util.inherits(Strategy, AbstractStrategy)

Strategy.prototype.authenticate = function (req) {
  const serviceURL = new URL(this.spURL)
  serviceURL.pathname = req.originalUrl
  // XXX: Not sure if it's right to remove these fields
  serviceURL.searchParams.delete('ticket')
  serviceURL.search = ''
  const service = serviceURL.toString()

  const { ticket } = req.query
  if (!ticket) {
    const redirectURL = new URL(this.idpURL)
    redirectURL.pathname = '/login'
    redirectURL.searchParams.set('service', service)

    // Send them to their CAS SSO page
    return this.redirect(redirectURL.toString())
  }

  const self = this
  function doneCallback(err, user, info) {
    if (err) {
      return self.error(err)
    }
    if (!user) {
      return self.fail(info)
    }
    self.success(user, info)
  }

  const authURL = new URL(this.idpURL)
  authURL.pathname = '/p3/serviceValidate'
  authURL.searchParams = new URLSearchParams({ ticket, service })

  fetch(authURL)
    .then(res => res.text())
    .then(body => parseString(body, xmlParseOptions))
    .then(xml => {
      const response = xml.serviceresponse
      const { authenticationsuccess, authenticationfailure } = response

      if (authenticationfailure) {
        const errorCode = authenticationfailure.$.code
        return doneCallback(new Error('Authentication failed ' + errorCode))
      }
      if (authenticationsuccess) {
        self._verify(authenticationsuccess, doneCallback)
        return
      }
      return doneCallback(new Error('Authentication failed'))
    })
    .catch(err => {
      return self.fail(new Error('CAS Error:' + err))
    })
}

module.exports = { Strategy }
