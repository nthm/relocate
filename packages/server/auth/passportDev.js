const AbstractStrategy = require('passport-strategy')
const util = require('util')

function Strategy(options) {
  AbstractStrategy.call(this)
  if (!options.username || !options.key) {
    throw 'Dev Passport: Must provide a `username` and `key`'
  }
  this.name = 'dev'
  this._username = options.username
  this._key = options.key
  this._keyField = options.keyField || 'key'
}

util.inherits(Strategy, AbstractStrategy)

Strategy.prototype.authenticate = function (req) {
  const self = this
  const key = req.body[this._keyField]
  if (!key) {
    return this.fail({ message: `No ${this._keyField} in response` }, 400)
  }
  if (key !== this._key) {
    return self.fail()
  }
  self.success(this._username)
}

module.exports = { Strategy }
