# Tiling, and ImageOverlay vs TileLayer

Because the images, y'know, they're just not _that_ large.

It's 630kb (zopfli + gz) for an entire floor as one image. Meanwhile all tiles
for one floor is 1.2mb (zopfli + tar.gz). Note however, that's it's not likely
someone will ever request all tiles of all zoom levels for a floor.

So how good is the browser at moving and zooming an image a bit larger than
6000x3000? Not great, actually.

It defenitely works, and because it's so little code to change, I will
definitely use an ImageOverlay instead of tiles for testing out exports from
GIMP. However, there's a very noticeable lag when zooming, and you can clearly
see pixels in the image as it's rerendering.

Under the hood, Leaflet is doing everything right: the image is in it's own
layer managed by the GPU and it's moved via `transform: translate3d()`. Still
not as great as tiles.

I'll push the changes to a branch.

I took the opportunity to try creating a border around the transparent image
using the CSS `filter: drop-shadow()` property. Originally I tried this with
tiles and it didn't work since there was no way to prevent the browser from
drawing around each tile and not just areas bordering transparent pixels. For a
full image however, it works..kinda:

TODO: Add screenshot

The CSS property isn't fully supported, and it doesn't have a spread attribute
like `box-shadow`, so you can't grow the border. People have suggested chaining
4 drop shadows for all 4 directions, and it works but is very slow. It's also
clear that the shadow is being recomputed on each zoom, and overlapping in
strange ways. Worth a shot.

## gdal2tiles-leaflet

In the future, as more buildings are done, I'd like to convert the backend
scripts (`backend/`) to NodeJS and remove Python as a dependency. It's not super
straight forward for someone to setup - they'll need `pip`, `virtualenv`, and
possibly need to download libraries like `requests` or `lxml` manually as I
needed to do.

For tiling, everything was done using `gdal2tiles-leaflet` which is a
fork/extension to an already ~3000 line Python 2 script for breaking up an image
into tiles - thankfully it's well documented.

While searching for alternatives, someone reminded me it's not hard to roll your
own in a very small amount of code. Most of the work can be handled by
ImageMagick which should be easily installed on Linux machines (more so than
setting up Python at least). 

The script, `gdal2tiles`. Also see the multiprocess version (_so much faster_):
- https://github.com/commenthol/gdal2tiles-leaflet/blob/master/gdal2tiles.py

A NodeJS API to ImageMagick and GraphicMagick:
- https://github.com/aheckmann/gm

The post about rolling your own tiler, describing the algorithm. Links to a Ruby
version:
- http://omarriott.com/aux/leaflet-js-non-geographical-imagery/

ImageMagick can also do tiling via the `convert` CLI tool, which is neat. Note,
I tried bordering the image with:

```sh
convert normal.png \
  -bordercolor none -border 10x10 \
  -background red -alpha background -channel A \
  -blur 0x10 -level 0,70% \
  output.png
```

Which __isn't__ bordering, but rather, blurring out a new black background layer
mask... I think. Played with several settings but it always blurs the actual map
itself - apparently I'd need to merge down the original on top.

Deciding to just use GIMP rounded bordering for now.
